package fr.anebris.buywarp.v3.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import fr.anebris.buywarp.v3.interfaces.IConfigRepository;
import fr.anebris.buywarp.v3.interfaces.IDatabaseService;

public class DatabaseService implements IDatabaseService {
	
	private final IConfigRepository configRepository;
	private static Connection connection;
	private String urlBase;
	private String host;
	private String database;
	private String userName;
	private String password;
	
	public DatabaseService(IConfigRepository configRepository) {
		this.configRepository = configRepository;
		this.getMysqlConfig();
		this.connexion();
	}
	
	public void getMysqlConfig() {
		this.urlBase = "jdbc:mysql://";
		this.host = configRepository.getMysqlHost();
		this.database = configRepository.getMysqlDatabase();
		this.userName = configRepository.getMysqlUsername();
		this.password = configRepository.getMysqlPassword();
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public void checkDatabase() {
		try {
			PreparedStatement preparedStatement = this.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS bw_buywarp ("
					+ "id integer PRIMARY KEY NOT NULL AUTO_INCREMENT,"
					+ "warp_name varchar(30) NOT NULL,"
					+ "location varchar(255) NOT NULL,"
					+ "player_uuid varchar(255) NOT NULL,"
					+ "creation_datetime datetime NOT NULL,"
					+ "expiration_datetime datetime NOT NULL"
					+ ");");
			preparedStatement.execute();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void connexion() {
		if (!isOnline()) {
			try {
				connection = DriverManager.getConnection(this.urlBase + this.host + "/" + this.database+"?autoReconnect=true&useSSL=false", this.userName, this.password);
				this.checkDatabase();
				return;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void deconnexion() {
		if (isOnline()) {
			try {
				connection.close();
				return;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean isOnline() {
		try {
			if ((connection == null) || (connection.isClosed())) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
