package fr.anebris.buywarp.v3.services;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.enums.InventoryType;
import fr.anebris.buywarp.v3.interfaces.IInventoryService;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.IWarpService;
import fr.anebris.buywarp.v3.plugins.ItemBuilder;

public class InventoryService implements IInventoryService {

    private final IWarpService warpService;
    private final IMessageService messageService;
    
	private final HashMap<InventoryType, String> inventories = new HashMap<InventoryType, String>(); 

    public InventoryService(IWarpService warpService, IMessageService messageService) {
        this.warpService = warpService;
        this.messageService = messageService;
        
        this.inventories.put(InventoryType.WARP_LIST, messageService.getMessage(ConfigMessage.INV_WARP_LIST, false));
    }

	@Override
	public int getInventorySize(int size) {
		if (size > 54)
			return 54;
		if (size == 0)
			return 9;
		return (int) (Math.ceil(size / 9d) * 9);
	}

	@Override
	public boolean isCustomInventory(InventoryType storedInventory, String inventory) {
		return inventories.get(storedInventory) == inventory;
	}
	
	@Override
	public Inventory getWarpListInventory() {
		HashMap<String, List<Object>> warps = warpService.getWarps();
		
		Inventory inv = Bukkit.createInventory(null, getInventorySize(warps.size()), inventories.get(InventoryType.WARP_LIST));

		for (Entry<String, List<Object>> warp : warps.entrySet()) {
			List<String> lore = messageService.getListMessage(ConfigMessage.ITEM_WARP_LORE, false);
			
			UUID uuid = UUID.fromString(warp.getValue().get(0).toString());
			String owner = Bukkit.getOfflinePlayer(uuid).getName();
			
			for (int i = 0; i < lore.size(); i++) {
				lore.set(i, lore.get(i)
						.replace("{owner}", owner)
						.replace("{expiration}", new SimpleDateFormat("dd/MM/yyyy - HH.mm.ss").format(warp.getValue().get(3)) ));
			}
			inv.addItem(new ItemBuilder(Material.PLAYER_HEAD)
					.setSkullOwner(owner)
					.setName(messageService.getMessage(ConfigMessage.ITEM_WARP_NAME, false).replace("{warp_name}", warp.getKey().toString()))
					.setLore(lore)
					.toItemStack());
		}
		return inv;	
	}

	@Override
	public Inventory getConfirmInventory(Player player, String title, double amount) {
        Inventory inv = Bukkit.createInventory(player, 9*3, title);
        
        List<String> rejectLore = messageService.getListMessage(ConfigMessage.ITEM_REJECT_LORE, false);
		List<String> acceptLore = messageService.getListMessage(ConfigMessage.ITEM_ACCEPT_LORE, false);

		for (int i = 0; i < acceptLore.size(); i++) {
			acceptLore.set(i, acceptLore.get(i)
					.replace("{cost}", String.valueOf((int) amount)));
		}
		
        inv.setItem(9+3, new ItemBuilder(Material.RED_CONCRETE).setName(messageService.getMessage(ConfigMessage.ITEM_REJECT_NAME, false)).setLore(rejectLore).toItemStack());
        inv.setItem(9+5, new ItemBuilder(Material.GREEN_CONCRETE).setName(messageService.getMessage(ConfigMessage.ITEM_ACCEPT_NAME, false)).setLore(acceptLore).toItemStack());

        return inv;
	}
}
