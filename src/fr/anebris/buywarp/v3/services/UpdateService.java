package fr.anebris.buywarp.v3.services;

import fr.anebris.buywarp.v3.buywarp.Main;
import fr.anebris.buywarp.v3.interfaces.IUpdateRepository;
import fr.anebris.buywarp.v3.interfaces.IUpdateService;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class UpdateService implements IUpdateService {

    private String cachedUpdateVersion;
    private final IUpdateRepository updateRepository;
    private final Main main;
    private Date lastChecked;

    public UpdateService(IUpdateRepository updateRepository, Main main) {

        this.updateRepository = updateRepository;
        this.main = main;
    }


    @Override
    public boolean hasUpdate() {
        return hasUpdate(this.getCurrentVersion());
    }

    @Override
    public boolean hasUpdate(String version) {

        if (lastChecked == null) {
            lastChecked = new Date();
        }

        if (cachedUpdateVersion == null) {
            cachedUpdateVersion = updateRepository.getLatestVersion();
        }

        double difference = (new Date().getTime() - lastChecked.getTime());

        if (difference > 7200000) {
            lastChecked = new Date();
            cachedUpdateVersion = updateRepository.getLatestVersion();
        }

        if(cachedUpdateVersion == null)
            return false;


        if(cachedUpdateVersion.equals(version))
            return false;

        try{

            List<Integer> splittedCurrentVersion = Arrays.stream(getCurrentVersion().split("\\.")).map(Integer::parseInt).collect(Collectors.toList());
            List<Integer> splittedCachedVersion = Arrays.stream(this.cachedUpdateVersion.split("\\.")).map(Integer::parseInt).collect(Collectors.toList());


            for(int i = 0; i < splittedCachedVersion.size(); i ++){

                Integer current = splittedCurrentVersion.get(i);
                Integer cached = splittedCachedVersion.get(i);

                if(current > cached)
                    return false;

            }

            return true;

        }catch(Exception ex){
            return false;
        }

    }

    @Override
    public String getCurrentVersion() {
        return main.getDescription().getVersion();
    }

    @Override
    public String getCachedUpdateVersion(){
        return this.cachedUpdateVersion;
    }

}