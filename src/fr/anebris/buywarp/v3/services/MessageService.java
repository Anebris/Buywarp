package fr.anebris.buywarp.v3.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.command.CommandSender;
import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.interfaces.IConfigRepository;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.statics.ConfigMessageMapper;
import fr.anebris.buywarp.v3.statics.Message;

public class MessageService implements IMessageService {

    private final IConfigRepository configRepository;
    
    public MessageService(IConfigRepository configRepository) {
        this.configRepository = configRepository;
      
    }

    @Override
    public String getMessage(ConfigMessage message, boolean includePrefix) {

        //String prefix = configRepository.getPrefix();
        String messagePath = ConfigMessageMapper.getMapper().getMessagePath(message);
        String configMessage = configRepository.getString(messagePath);

        return this.getMessage(configMessage, includePrefix);
    }

	@Override
	public List<String> getListMessage(ConfigMessage message, boolean includePrefix) {
        String messagePath = ConfigMessageMapper.getMapper().getMessagePath(message);
        List<String> configMessage = configRepository.getListString(messagePath);

        return this.getListMessage(configMessage, includePrefix);
	}
	
    @Override
    public String getMessage(String message, boolean includePrefix) {
        String prefix = configRepository.getPrefix();
        if(message.length() == 0)
            return "";

        if(prefix.length() > 0 && includePrefix){
            return String.format("%s %s", prefix, message);
        }

        return Message.getMessage(message.trim());
    }

    @Override
    public List<String> getListMessage(List<String> message, boolean includePrefix) {
        String prefix = configRepository.getPrefix();
        if(message.size() == 0) {
        	message.add("");
            return message;
        }
        
        if (prefix.length() > 0 && includePrefix){
			return Message.getListMessage(message.stream().map(msg -> prefix + " " + Message.getMessage(msg)).collect(Collectors.toList()));        	
        }
        
        return Message.getListMessage(message);  
    }

	public void sendMessage(CommandSender sender, String message, boolean showPrefix){

        String prefix = configRepository.getPrefix();

        if(message.length() == 0)
            return;

        String fullMessage;
        fullMessage = message;

        if(showPrefix && prefix.length() > 0)
        fullMessage = String.format("%s %s", prefix, message);

        sender.sendMessage(Message.getMessage(fullMessage));
    }

	@Override
	public List<String> autoComplete(List<String> tmpList, String arg ) {
    	List<String> list = new ArrayList<>();
        for(int i = 0; i < tmpList.size(); i++) {
        	if (tmpList.get(i).toLowerCase().contains(arg.toLowerCase())) {
    	    	list.add(tmpList.get(i));
        	}
        }
        return list;		
	}
}