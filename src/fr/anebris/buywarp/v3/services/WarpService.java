package fr.anebris.buywarp.v3.services;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.earth2me.essentials.Essentials;
import fr.anebris.buywarp.v3.interfaces.IConfigRepository;
import fr.anebris.buywarp.v3.interfaces.IWarpRepository;
import fr.anebris.buywarp.v3.interfaces.IWarpService;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;

public class WarpService implements IWarpService {
	
	private final IWarpRepository warpRepository;
	private final IConfigRepository configRepository;
	private final Essentials ess = (Essentials) Bukkit.getServer().getPluginManager().getPlugin("Essentials");

	private static Economy eco = (Economy) Bukkit.getServer().getServicesManager().getRegistration(Economy.class).getProvider();
	private static HashMap<String, List<Object>> warpList = new HashMap<String, List<Object>>();
	
	public WarpService(IWarpRepository warpRepository, IConfigRepository configRepository) {
		this.warpRepository = warpRepository;
		this.configRepository = configRepository;
		this.initializeWarps();
	}

	@Override
	public void initializeWarps() {
		WarpService.warpList = warpRepository.getWarps();
	}
	
	@Override
	public void setWarp(String warpName, UUID player, Location location) throws Exception {
		Timestamp creationDatetime = new Timestamp(System.currentTimeMillis());
		Timestamp expirationDatetime = (Timestamp) configRepository.getWarpExpirationDatetime(creationDatetime, configRepository.getPlayerType(Bukkit.getPlayer(player)));
		
		ess.getWarps().setWarp(warpName, location);
		warpList.put(warpName, Arrays.asList(player, location, creationDatetime, expirationDatetime));
		warpRepository.setWarp(warpName, player, location, creationDatetime, expirationDatetime);
	}

	@Override
	public void delWarp(String warpName) throws Exception {
		if (ess.getWarps().getWarp(warpName) != null) {
			ess.getWarps().removeWarp(warpName);
		}
		warpList.remove(warpName);
		warpRepository.delWarp(warpName);
	}

	@Override
	public void renameWarp(String oldName, String newName) throws Exception {
		ess.getWarps().setWarp(newName, ess.getWarps().getWarp(oldName).clone());
		ess.getWarps().removeWarp(oldName); 
		warpList.put(newName, warpList.get(oldName));
		warpList.remove(oldName);
		warpRepository.renameWarp(oldName, newName);
	}
	
	@Override
	public void renewWarp(String warpName, Player player) {
		Timestamp oldExpirationDatetime = Timestamp.valueOf(warpList.get(warpName).get(3).toString());
		Timestamp newExpirationDatetime = (Timestamp) configRepository.getWarpExpirationDatetime(oldExpirationDatetime, configRepository.getPlayerType(player));
		warpRepository.renewWarp(warpName, newExpirationDatetime);
		warpList.get(warpName).set(3, newExpirationDatetime);
	}
	
	@Override
	public HashMap<String, List<Object>> getWarps() {
		return warpList;
	}
	
	@Override
	public HashMap<String, List<Object>> getPlayerWarps(Player player) {
		HashMap<String, List<Object>> playerWarps = new HashMap<String, List<Object>>();
		for (Entry<String, List<Object>> warp : warpList.entrySet()) {
			if (player.getUniqueId().compareTo(getWarpOwner(warp.getKey())) == 0) {
				playerWarps.put(warp.getKey(), warp.getValue());
			}
		}
		return playerWarps;
	}
	
	@Override
	public int getWarpLimit(Player player) {
		for (int i = 999; i >= 0; i--) {
			if (Bukkit.getPlayer("").hasPermission(String.format("bw.warpLimit.%s", i))) return i;
		}
		return -1;
	}
	
	
	
	
	
	
	/* ######################## */
	@Override
	public boolean isExistingWarp(String warp_name) {
		return warpList.containsKey(warp_name) && ess.getWarps().getList().contains(warp_name) ? true : false;
	}
	
	@Override
	public boolean isValidName(String warp_name) {
		return Pattern.matches("^[0-9]*$", warp_name) == false;
	}
	/* ######################## */
	
	
	
	
	
	@Override
	public void setWarpOwner(String warp_name, UUID playerUUID) {
		warpList.get(warp_name).set(0, playerUUID);
		warpRepository.setOwner(warp_name, playerUUID);
	}
	
	@Override
	public UUID getWarpOwner(String warp_name) {
		return UUID.fromString(warpList.get(warp_name).get(0).toString());
	}
	

	public void setWarpLocation(String warp_name, Location location) {
		
	}
	
	
	public Location getWarpLocation(String warp_name) {
		return null;
	}
	

	public void setWarpCreation(String warp_name, Timestamp expiration) {
		
	}
	
	
	public Timestamp getWarpCreation(String warp_name) {
		return null;
	}
	
	
	public void setWarpExpiration(String warp_name, Timestamp expiration) {
		
	}
	
	
	public Timestamp getWarpExpiration(String warp_name) {
		return null;
	}
	
	
	
	
	
	/* ######################## */
	@Override
	public boolean playerHasEnoughMoney(Player player, double price) {
		return eco.getBalance(player) - price >= 0.0D;
	}
	
	@Override
	public EconomyResponse takeMoney(Player player, double price) {
		return eco.withdrawPlayer(player, price);
	}
	/* ######################## */
	
}
