package fr.anebris.buywarp.v3.runnables;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.scheduler.BukkitRunnable;

import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.IWarpService;

public class WarpTimer extends BukkitRunnable  {

	private final IWarpService warpService;
	private final IMessageService messageService;

	public WarpTimer(IWarpService warpService, IMessageService messageService) {
		this.warpService = warpService;
		this.messageService = messageService;
	}

	@Override
	public void run() {

		for (Entry<String, List<Object>> warp : warpService.getWarps().entrySet()) {
			String warpName = warp.getKey().toString();
			Timestamp actualDate = new Timestamp(System.currentTimeMillis());
			try {
				Date expirationDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").parse(warp.getValue().get(3).toString());
				if ((actualDate.compareTo(expirationDate) > 0) || (!warpService.isExistingWarp(warpName))) {
					try {
						warpService.delWarp(warpName);
						OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(warp.getValue().get(0).toString()));
						if (player.isOnline()) {
							messageService.sendMessage(player.getPlayer(), messageService.getMessage(ConfigMessage.WARP_DELETED, true).replace("{warp_name}", warpName), false);
						} else {
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "mail send " + player.getName() + " " +  messageService.getMessage(ConfigMessage.WARP_DELETED, false).replace("{warp_name}", warpName));
						}
						System.out.println("[BuyWarp] warp expired : " + warpName);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
	    }
	}

}
