package fr.anebris.buywarp.v3.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;

import fr.anebris.buywarp.v3.interfaces.IDatabaseService;
import fr.anebris.buywarp.v3.interfaces.IWarpRepository;

public class WarpRepository implements IWarpRepository {
	
	private final IDatabaseService databaseService;

	public WarpRepository( IDatabaseService databaseService) {
		this.databaseService = databaseService;
	}

	@Override
	public void setWarp(String warpName, UUID playerUUID, Location location, Timestamp creationDatetime, Timestamp expirationDatetime) {
		String loc = location.getWorld().getName() + ":" + location.getBlockX() + ":" + location.getBlockY() + ":" + location.getBlockZ();
		try {
			PreparedStatement preparedStatement = databaseService.getConnection().prepareStatement("INSERT INTO bw_buywarp (warp_name, location, player_uuid, creation_datetime, expiration_datetime) VALUES (?, ?, ?, ?, ?)");
			preparedStatement.setString(1, warpName);
			preparedStatement.setString(2, loc);
			preparedStatement.setString(3, playerUUID.toString());
			preparedStatement.setTimestamp(4, creationDatetime);
			preparedStatement.setTimestamp(5, expirationDatetime);
			preparedStatement.execute();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void delWarp(String warpName) {
		try {
			PreparedStatement preparedStatement = databaseService.getConnection().prepareStatement("DELETE FROM bw_buywarp WHERE warp_name = ?");
			preparedStatement.setString(1, warpName);
			preparedStatement.execute();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void renewWarp(String warpName, Timestamp expirationDatetime) {
		try {
			PreparedStatement preparedStatement = databaseService.getConnection().prepareStatement("UPDATE bw_buywarp SET expiration_datetime = ? WHERE warp_name = ?");
			preparedStatement.setTimestamp(1, expirationDatetime);
			preparedStatement.setString(2, warpName);
			preparedStatement.execute();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public HashMap<String, List<Object>> getWarps() {
		HashMap<String, List<Object>> warpList = new HashMap<String, List<Object>>();
		
		try {
			PreparedStatement preparedStatement = databaseService.getConnection().prepareStatement("SELECT * FROM bw_buywarp");
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				String warpName = rs.getString("warp_name");
				String player = rs.getString("player_uuid");
				String location = rs.getString("location");
				Timestamp creationDatetime = rs.getTimestamp("creation_datetime");
				Timestamp expirationDatetime = rs.getTimestamp("expiration_datetime");
				warpList.put(warpName, Arrays.asList(player, location, creationDatetime, expirationDatetime));
			}
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return warpList;
	}

	@Override
	public void setOwner(String warpName, UUID playerUUID) {
		try {
			PreparedStatement preparedStatement = databaseService.getConnection().prepareStatement("UPDATE bw_buywarp SET player_uuid = ? WHERE warp_name = ?");
			preparedStatement.setString(1, playerUUID.toString());
			preparedStatement.setString(2, warpName);
			preparedStatement.execute();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void renameWarp(String oldName, String newName) {
		try {
			PreparedStatement preparedStatement = databaseService.getConnection().prepareStatement("UPDATE bw_buywarp SET warp_name = ? WHERE warp_name = ?");
			preparedStatement.setString(1, newName);
			preparedStatement.setString(2, oldName);
			preparedStatement.execute();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
