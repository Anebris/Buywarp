package fr.anebris.buywarp.v3.repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.bukkit.entity.Player;

import fr.anebris.buywarp.v3.buywarp.Main;
import fr.anebris.buywarp.v3.interfaces.IConfigRepository;

public class ConfigRepository implements IConfigRepository {

    private final Main main;
    
    public ConfigRepository(Main main) {
        this.main = main;
    }
    
    @Override
    public String getMysqlHost() {
    	return main.getConfig().getString("mysql.host");
    }
 
    @Override
    public String getMysqlDatabase() {
    	return main.getConfig().getString("mysql.database");
    }
    
    @Override
    public String getMysqlUsername() {
    	return  main.getConfig().getString("mysql.username");
    }
    
    @Override
    public String getMysqlPassword() {
    	return main.getConfig().getString("mysql.password");
    }
    
    @Override
    public String getPrefix() {
        return main.getConfig().getString("messages.prefix");
    }

    @Override
    public String getString(String string) {
        return main.getConfig().getString(string);
    }

    @Override
    public List<String> getListString(String string) {
        return main.getConfig().getStringList(string);
    }
    
    @Override
	public void reloadConfig() {
		 main.reloadConfig();
	}
	
	@Override
	public String getPlayerType(Player player) {
		
		ArrayList<String> warpTypes = new ArrayList<String>();
		for (String key : main.getConfig().getConfigurationSection("warp").getKeys(false)) {
			warpTypes.add(key);
		}

		Collections.reverse(warpTypes);
		for (String type : warpTypes ) {
			if (player.hasPermission(String.format("buywarp.buy.%s", type) )) {
				return type;
			}
		}
		
		return warpTypes.get(warpTypes.size() - 1 );
	}
	
	@Override
	public int getMaxWarp(String playerType) {
		return main.getConfig().getInt(String.format("warp.%s.max", playerType));
	}
	
	@Override
	public double getWarpCost(String playerType) {
		return main.getConfig().getDouble(String.format("warp.%s.cost", playerType));
	}
	
	@Override
	public Date getWarpExpirationDatetime(Timestamp referedDatetime, String playerType) {
		String configDuration = main.getConfig().getString(String.format("warp.%s.duration", playerType));
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(referedDatetime);
		for ( String s : configDuration.split("(?<=[a-zA-Z])")) {
			
			if (s.contains("M")) {
				calendar.add(Calendar.MONTH, Integer.parseInt(s.replace("M", "")));
			}
			if (s.contains("d")) {
				calendar.add(Calendar.DATE, Integer.parseInt(s.replace("d", "")));
			}
			if (s.contains("h")) {
				calendar.add(Calendar.HOUR, Integer.parseInt(s.replace("h", "")));
			}
			if (s.contains("s")) {
				calendar.add(Calendar.SECOND, Integer.parseInt(s.replace("s", "")));
			}
			
		}
		return new Timestamp(calendar.getTimeInMillis());
	}
}
