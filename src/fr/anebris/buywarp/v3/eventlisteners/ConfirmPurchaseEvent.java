package fr.anebris.buywarp.v3.eventlisteners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.util.Consumer;

import fr.anebris.buywarp.v3.interfaces.IInventoryService;
import fr.anebris.buywarp.v3.statics.Bootstrapper;

public class ConfirmPurchaseEvent implements Listener {
	
	private final IInventoryService inventoryService;

    private Player p;
    private Consumer<Boolean> bi;

	
	public ConfirmPurchaseEvent(Player p, String title, double amount, Consumer<Boolean> true_false) {
    	
		Bukkit.getPluginManager().registerEvents(this, Bootstrapper.getBootstrapper().getMain());
		this.inventoryService = Bootstrapper.getBootstrapper().getInventoryService();
		
        this.p = p;
        this.bi = true_false;
        
        p.openInventory(inventoryService.getConfirmInventory(p, title, amount));
    }

    @EventHandler
    private void onClick(InventoryClickEvent e) {

        if (e.getView().getTopInventory().getHolder() != this.p) return;

        e.setCancelled(true);

        if (e.getClickedInventory() == null || e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR)  return;

        if (e.getClickedInventory().getHolder() != this.p) return;

        switch (e.getSlot()) {
	        case 9+3:
	            bi.accept(false);
	            break;
	
	        case 9+5:
	            bi.accept(true);
	            break;
	
	        default:
	            break;
        }

    }

    @EventHandler
    private void onClose(InventoryCloseEvent e) {

        if (e.getInventory().getHolder() == this.p) {

            InventoryClickEvent.getHandlerList().unregister(this);
            InventoryCloseEvent.getHandlerList().unregister(this);

        }

    }

}