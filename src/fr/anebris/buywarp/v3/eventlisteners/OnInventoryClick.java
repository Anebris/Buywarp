package fr.anebris.buywarp.v3.eventlisteners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.anebris.buywarp.v3.enums.InventoryType;
import fr.anebris.buywarp.v3.interfaces.IInventoryService;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.IWarpService;
import net.md_5.bungee.api.ChatColor;

public class OnInventoryClick implements Listener {

	@SuppressWarnings("unused")
	private final IWarpService warpService;
	@SuppressWarnings("unused")
	private final IMessageService messageService;
	private final IInventoryService inventoryService;
	
	public OnInventoryClick(IWarpService warpService, IMessageService messageService, IInventoryService inventoryService) {	
		this.warpService = warpService;
		this.messageService = messageService;
		this.inventoryService = inventoryService;
	}

    @SuppressWarnings("unused")
	@EventHandler
    public void onClick(InventoryClickEvent e) throws Exception{

        if (e.getView().getTitle() == null)
        	return;

        for (InventoryType inv : InventoryType.values()) {
        	if (inventoryService.isCustomInventory(inv , e.getView().getTitle().toString())) {
            	e.setCancelled(true);

            	Player player = (Player) e.getWhoClicked();

                if (e.getClickedInventory() == null || e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR)  return;

        		//items des warps
        		if (e.getCurrentItem().getType().equals(Material.PLAYER_HEAD)) {
        			player.performCommand("warp "+ ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()));
        			player.closeInventory();
        		}
            } 	
        }
    }
}
