package fr.anebris.buywarp.v3.eventlisteners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.IUpdateService;

public class PlayerJoinEventListener implements Listener {

    private final IMessageService messageService;
    private final IUpdateService updateService;
    private final Plugin plugin;


    public PlayerJoinEventListener(Plugin plugin, IUpdateService updateService, IMessageService messageService) {
        this.plugin = plugin;
        this.updateService = updateService;
        this.messageService = messageService;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {

        Player player = e.getPlayer();

        if (!player.hasPermission("bw.admin.update"))
            return;

        Bukkit.getScheduler().runTaskAsynchronously(plugin, () ->
        {
            if(updateService.hasUpdate())
                messageService.sendMessage(player, messageService.getMessage(ConfigMessage.UPDATE_FOUND, true).replace("{old_version}", updateService.getCurrentVersion()).replace("{new_version}", updateService.getCachedUpdateVersion()), false);
        });
    }
    
}
