package fr.anebris.buywarp.v3.statics;

import java.util.HashMap;

import org.bukkit.ChatColor;

import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.buywarp.Main;

public class ConfigMessageMapper {

	private static ConfigMessageMapper messageMapper;
	private final HashMap<ConfigMessage, String> configMessages;
	private Main main;
	
	private ConfigMessageMapper(){
		configMessages = new HashMap<>();
		configMessages.put(ConfigMessage.PREFIX, "messages.prefix");
		configMessages.put(ConfigMessage.WARP_CREATED, "messages.warp-created");
		configMessages.put(ConfigMessage.WARP_RENEWED, "messages.warp-renewed");
		configMessages.put(ConfigMessage.WARP_DELETED, "messages.warp-deleted");
		configMessages.put(ConfigMessage.WARP_ALREADY_IN_USE, "messages.warp-already-in-use");
		configMessages.put(ConfigMessage.WARP_ALREADY_OWNED, "messages.warp-already-owned");
		configMessages.put(ConfigMessage.WARP_DOESNT_EXIST, "messages.warp-doesnt-exist");
		configMessages.put(ConfigMessage.WARP_NAME_IS_INVALID, "messages.warp-name-is-invalid");
		configMessages.put(ConfigMessage.WARP_LIMITED, "messages.warp-limited");
		configMessages.put(ConfigMessage.WARP_SET_OWNER, "messages.warp-set-owner");
		configMessages.put(ConfigMessage.WARP_NEW_OWNER, "messages.warp-new-owner");
		configMessages.put(ConfigMessage.WARP_RENAMED, "messages.warp-renamed");
		configMessages.put(ConfigMessage.NOT_ENOUGH_MONEY, "messages.not-enough-money");
		configMessages.put(ConfigMessage.NOT_THE_OWNER, "messages.not-the-owner");
		configMessages.put(ConfigMessage.PLAYER_IS_OFFLINE, "messages.player-is-offline");
		configMessages.put(ConfigMessage.CMD_RELOAD, "messages.cmd-reload");
		configMessages.put(ConfigMessage.NO_PERMISSION, "messages.no-permission");
		configMessages.put(ConfigMessage.UPDATE_FOUND, "messages.update-found");
		
		configMessages.put(ConfigMessage.INV_CONFIRM_BUY, "inventories.confirmBuy");
		configMessages.put(ConfigMessage.INV_CONFIRM_RENEW, "inventories.confirmRenew");
		configMessages.put(ConfigMessage.INV_WARP_LIST, "inventories.warpList");
		
		configMessages.put(ConfigMessage.ITEM_WARP_NAME, "items.warps.name");
		configMessages.put(ConfigMessage.ITEM_WARP_LORE, "items.warps.lore");
		
		configMessages.put(ConfigMessage.ITEM_ACCEPT_NAME, "items.accept.name");
		configMessages.put(ConfigMessage.ITEM_ACCEPT_LORE, "items.accept.lore");
		configMessages.put(ConfigMessage.ITEM_REJECT_NAME, "items.reject.name");
		configMessages.put(ConfigMessage.ITEM_REJECT_LORE, "items.reject.lore");
	}

	public static ConfigMessageMapper getMapper(){
		if(messageMapper == null)
			messageMapper = new ConfigMessageMapper();
			return messageMapper;
	}

	public void initialize(Main main){
		this.main = main;
	}

	public String getMessagePath(ConfigMessage message){
		return configMessages.get(message);
	}

	public String getMessage(ConfigMessage configMessage, Boolean includePrefix) {
		
		if(includePrefix)
			return ChatColor.translateAlternateColorCodes('&', getMessage(ConfigMessage.PREFIX, false)+ " " + main.getConfig().getString(configMessages.get(configMessage)));
		return ChatColor.translateAlternateColorCodes('&', main.getConfig().getString(configMessages.get(configMessage)));
	}
	
	public String getListMessage(ConfigMessage configMessage, Boolean includePrefix) {
		
		//if(includePrefix)
			return ChatColor.translateAlternateColorCodes('&', getMessage(ConfigMessage.PREFIX, false)+ " " + main.getConfig().getList(configMessages.get(configMessage)));
		//return ChatColor.translateAlternateColorCodes('&', main.getConfig().getString(configMessages.get(configMessage)));
	}
}
