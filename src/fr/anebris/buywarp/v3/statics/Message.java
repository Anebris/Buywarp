package fr.anebris.buywarp.v3.statics;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;

public class Message {
	
    public static String getMessage(String message){
        return ChatColor.translateAlternateColorCodes('&', message);
    }

	public static List<String> getListMessage(List<String> messages) {	
		return messages.stream().map(msg -> ChatColor.translateAlternateColorCodes('&', msg) ).collect(Collectors.toList());
	}

}
