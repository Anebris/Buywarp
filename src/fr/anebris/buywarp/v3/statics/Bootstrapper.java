package fr.anebris.buywarp.v3.statics;

import fr.anebris.buywarp.v3.interfaces.IConfigRepository;
import fr.anebris.buywarp.v3.repository.ConfigRepository;
import fr.anebris.buywarp.v3.repository.UpdateRepository;
import fr.anebris.buywarp.v3.repository.WarpRepository;
import fr.anebris.buywarp.v3.buywarp.Main;
import fr.anebris.buywarp.v3.interfaces.IDatabaseService;
import fr.anebris.buywarp.v3.interfaces.IInventoryService;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.IUpdateRepository;
import fr.anebris.buywarp.v3.interfaces.IUpdateService;
import fr.anebris.buywarp.v3.interfaces.IWarpRepository;
import fr.anebris.buywarp.v3.interfaces.IWarpService;
import fr.anebris.buywarp.v3.runnables.WarpTimer;
import fr.anebris.buywarp.v3.services.DatabaseService;
import fr.anebris.buywarp.v3.services.InventoryService;
import fr.anebris.buywarp.v3.services.MessageService;
import fr.anebris.buywarp.v3.services.UpdateService;
import fr.anebris.buywarp.v3.services.WarpService;

public class Bootstrapper {
	
	private Main main;
	private static Bootstrapper instance;
	
	private IMessageService messageService;
	private IDatabaseService databaseService;
	private IWarpRepository warpRepository;
	private IWarpService warpService;
	private IInventoryService inventoryService;
	private IConfigRepository configRepository;
	private IUpdateRepository updateRepository;
	private IUpdateService updateService;
	private ConfigMessageMapper configMessageMapper;
	
	public void initialize(Main main) {
		this.main = main;
		
		this.configRepository = new ConfigRepository(main);

		this.messageService = new MessageService(configRepository);
		this.databaseService = new DatabaseService(configRepository);
		this.warpRepository = new WarpRepository(databaseService);
		this.warpService = new WarpService(warpRepository, configRepository);
		this.inventoryService = new InventoryService(warpService, messageService);
		this.updateRepository = new UpdateRepository("63702");
        this.updateService = new UpdateService(updateRepository, main);
		this.configMessageMapper = ConfigMessageMapper.getMapper();
		configMessageMapper.initialize(main);
		
		new WarpTimer(warpService, messageService).runTaskTimer(main, 0L, 1200L);

	}

	public static Bootstrapper getBootstrapper() {
		if (instance == null) {
			instance = new Bootstrapper();
		}
		return instance;
	}


	public Main getMain() {
		return main;
	}
    public IConfigRepository getConfigRepository() {
        return configRepository;
    }

    public IMessageService getMessageService() {
        return messageService;
    }
  
	public IDatabaseService getDatabaseService() {
		return databaseService;
	}

	public IWarpRepository getWarpRepository() {
	    return warpRepository;
	}
	
	public IWarpService getWarpService() {
		return warpService;
	}

	public IInventoryService getInventoryService() {
		return inventoryService;
	}
	
    public IUpdateService getUpdateService() {
        return updateService;
    }

    public IUpdateRepository getUpdateRepository() {
        return updateRepository;
    }
}
