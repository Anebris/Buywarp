package fr.anebris.buywarp.v3.interfaces;

public interface IUpdateRepository {

	String getLatestVersion();

}
