package fr.anebris.buywarp.v3.interfaces;

import java.sql.Connection;

public interface  IDatabaseService {
	
	void getMysqlConfig();
	Connection getConnection();
	void connexion();
	void deconnexion();
	boolean isOnline();

}
