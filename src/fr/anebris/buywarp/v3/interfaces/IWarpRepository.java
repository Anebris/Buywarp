package fr.anebris.buywarp.v3.interfaces;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;

public interface IWarpRepository {

	void setWarp(String warpName, UUID playerUUID, Location coordinates, Timestamp creationDatetime, Timestamp expirationDatetime);
	void delWarp(String warpName);
	void renewWarp(String warpName, Timestamp expirationDatetime);
	void renameWarp(String oldName, String newName);

	HashMap<String, List<Object>> getWarps();
	void setOwner(String warpName, UUID playerUUID);
}
