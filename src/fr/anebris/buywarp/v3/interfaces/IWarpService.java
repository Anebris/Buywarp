package fr.anebris.buywarp.v3.interfaces;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import net.milkbowl.vault.economy.EconomyResponse;

public interface IWarpService {

	void initializeWarps();
	
	HashMap<String, List<Object>> getWarps();
	HashMap<String, List<Object>> getPlayerWarps(Player player);
	
	void setWarp(String warpName, UUID player, Location loc) throws Exception;
	void delWarp(String warpName) throws Exception;
	void renewWarp(String warpName, Player player);
	void renameWarp(String oldName, String newName) throws Exception;

	boolean isExistingWarp(String warpName);
	boolean isValidName(String warpName);

	int getWarpLimit(Player player);
	
	
	
	boolean playerHasEnoughMoney(Player player, double price);
	EconomyResponse takeMoney(Player player, double price);
	
	
	
	void setWarpOwner(String warp_name, UUID playerUUID);
	UUID getWarpOwner(String warp_name);

}
