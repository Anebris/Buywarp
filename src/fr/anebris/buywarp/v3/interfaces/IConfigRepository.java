package fr.anebris.buywarp.v3.interfaces;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.bukkit.entity.Player;

public interface IConfigRepository {

	String getMysqlHost();
	String getMysqlDatabase();
	String getMysqlUsername();
	String getMysqlPassword();
	
    String getPrefix();
	String getString(String messagePath);
	List<String> getListString(String messagePath);
	
	void reloadConfig();

	String getPlayerType(Player player);
	double getWarpCost(String playerType);
	Date getWarpExpirationDatetime(Timestamp referedDatetime, String playerType);
	int getMaxWarp(String playerType);

}