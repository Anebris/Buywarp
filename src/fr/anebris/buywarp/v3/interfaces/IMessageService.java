package fr.anebris.buywarp.v3.interfaces;

import java.util.List;

import org.bukkit.command.CommandSender;
import fr.anebris.buywarp.v3.enums.ConfigMessage;

public interface IMessageService {

	String getMessage(ConfigMessage message, boolean includePrefix);
	List<String> getListMessage(ConfigMessage message, boolean includePrefix);
	String getMessage(String message, boolean includePrefix);
	List<String> getListMessage(List<String> message, boolean includePrefix);
	void sendMessage(CommandSender sender, String message, boolean showPrefix);
	List<String> autoComplete(List<String> tmpList, String arg);

}
