package fr.anebris.buywarp.v3.interfaces;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import fr.anebris.buywarp.v3.enums.InventoryType;

public interface IInventoryService {
	
	int getInventorySize(int size);
	boolean isCustomInventory(InventoryType storedInventory, String inventory);	
	
	Inventory getWarpListInventory();
	Inventory getConfirmInventory(Player player, String title, double amount);

}
