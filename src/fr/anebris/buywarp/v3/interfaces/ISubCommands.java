package fr.anebris.buywarp.v3.interfaces;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public interface ISubCommands {

	boolean executeCommand(CommandSender sender, Command command, String label, String[] args) throws Exception;

}
