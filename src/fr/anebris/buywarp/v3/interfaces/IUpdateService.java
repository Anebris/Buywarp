package fr.anebris.buywarp.v3.interfaces;

public interface IUpdateService {

    boolean hasUpdate();
    boolean hasUpdate(String version);

    String getCurrentVersion();
    String getCachedUpdateVersion();
}