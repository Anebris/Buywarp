package fr.anebris.buywarp.v3.buywarp;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import fr.anebris.buywarp.v3.eventlisteners.OnInventoryClick;
import fr.anebris.buywarp.v3.eventlisteners.PlayerJoinEventListener;
import fr.anebris.buywarp.v3.interfaces.IUpdateService;
import fr.anebris.buywarp.v3.plugins.Metrics;
import fr.anebris.buywarp.v3.commands.BuywarpCommands;
import fr.anebris.buywarp.v3.statics.Bootstrapper;


public class Main extends JavaPlugin {
	
	@Override
	public void onEnable() {
		/* [Metrics] */
		new Metrics(this, 7457);
		
		/* [Configuration files] */
		saveDefaultConfig();
		
		/* [Bootstrapper] */
		Bootstrapper bootstrapper = Bootstrapper.getBootstrapper();
		bootstrapper.initialize(this);

		/* [Commands] */
		Bukkit.getPluginCommand("buywarp").setExecutor(new BuywarpCommands(bootstrapper.getWarpService(), bootstrapper.getMessageService(), bootstrapper.getInventoryService(), bootstrapper.getConfigRepository()));
		
		/* [Events] */
		Bukkit.getPluginManager().registerEvents(new OnInventoryClick(bootstrapper.getWarpService(), bootstrapper.getMessageService(), bootstrapper.getInventoryService()), this);
		Bukkit.getPluginManager().registerEvents(new PlayerJoinEventListener(this,  bootstrapper.getUpdateService(), bootstrapper.getMessageService()), this);
		
		/* [TabCompleter] */
		Bukkit.getPluginCommand("buywarp").setTabCompleter(new BuywarpCommands(bootstrapper.getWarpService(), bootstrapper.getMessageService(), bootstrapper.getInventoryService(), bootstrapper.getConfigRepository()));
		
		/* [Update] */
		Bukkit.getScheduler().runTaskAsynchronously(this, () -> notifyIfNewUpdateExists(bootstrapper.getUpdateService()));
	}
	
	private void notifyIfNewUpdateExists(IUpdateService updateService) 
	{
		if(updateService.hasUpdate()) {
			String update = "UPDATE FOUND : Buywarp version {new_version} is available (current version: {old_version})".replace("{new_version}", updateService.getCachedUpdateVersion()).replace("{old_version}", updateService.getCurrentVersion());
			getLogger().info(update);
		}
			
	}
}


/*
 * permissions :
 * 	bw.admin.*:
 * 	  bw.admin.reload
 * 	  bw.admin.update
 * 	  bw.admin.rename
 * 	  bw.admin.setowner
 * 
 * 	bw.commands.*:
 * 	  bw.renew
 * 	  bw.list
 * 	  bw.rename
 * 	  bw.setowner
 * 
 * 	bw.buy.<warp>
 * 	bw.limit.<#>
 * 	
 */

/*
 * TODO :
 * 	Remove
 */