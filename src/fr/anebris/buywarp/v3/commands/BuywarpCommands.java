package fr.anebris.buywarp.v3.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import fr.anebris.buywarp.v3.interfaces.IWarpService;
import fr.anebris.buywarp.v3.commands.subcommands.BuySubCommand;
import fr.anebris.buywarp.v3.commands.subcommands.ListSubCommand;
import fr.anebris.buywarp.v3.commands.subcommands.ReloadSubCommand;
import fr.anebris.buywarp.v3.commands.subcommands.RenewSubCommand;
import fr.anebris.buywarp.v3.commands.subcommands.TestSubCommand;
import fr.anebris.buywarp.v3.commands.subcommands.RenameSubCommand;
import fr.anebris.buywarp.v3.commands.subcommands.SetOwnerSubCommand;
import fr.anebris.buywarp.v3.interfaces.IConfigRepository;
import fr.anebris.buywarp.v3.interfaces.IInventoryService;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.ISubCommands;

public class BuywarpCommands implements CommandExecutor, TabCompleter {
	
	private final HashMap<String, ISubCommands> subCommands = new HashMap<>();
	private final IWarpService warpService;
	private final IMessageService messageService;
	private final IInventoryService inventoryService;
	private final IConfigRepository configRepository;
	
	private  void registerSubCommands() {
		/* [buy] */
		subCommands.put("buy", new BuySubCommand(this.warpService, this.messageService, this.configRepository));
		/* [renew] */
		subCommands.put("renew", new RenewSubCommand(this.warpService, this.messageService, this.configRepository));
		/* [list] */
		subCommands.put("list", new ListSubCommand(this.messageService, this.inventoryService));
		/* [set owner] */
		subCommands.put("setowner", new SetOwnerSubCommand(this.warpService, this.messageService));
		/* [version] */
		/* [rename] */
		subCommands.put("rename", new RenameSubCommand(this.warpService, this.messageService));
		/* [reload] */
		subCommands.put("reload", new ReloadSubCommand(this.warpService, this.messageService, this.configRepository));
		/* [test] */
		subCommands.put("test", new TestSubCommand(this.warpService, this.messageService));
	}

	public BuywarpCommands(IWarpService warpService, IMessageService messageService, IInventoryService inventoryService, IConfigRepository configRepository) {
		this.inventoryService = inventoryService;
		this.warpService = warpService;
		this.messageService = messageService;
		this.configRepository = configRepository;
		this.registerSubCommands();
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if (!(sender instanceof Player)) {
			return false;
		}
		
		String subCommand = args[0];
		try {
			return subCommands.getOrDefault(subCommand, new ErrorCommand()).executeCommand(sender, command, label, args);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
		
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {

		Player player = (Player) sender;
		
		if(args.length < 1)
			return null;

		String subCommand = args[0];
   		List<String> subCommands = this.subCommands.keySet().stream().collect(Collectors.toList());
   	
	   	if (player.hasPermission(String.format("bw.%s", subCommand)) || player.hasPermission(String.format("bw.admin.%s", subCommand))) {
		   	if(args.length == 1){
		        Collections.sort(subCommands);
		        return messageService.autoComplete(subCommands, subCommand);
		    }
		   	
			switch (subCommand) {
			case "renew":
			   	if(args.length == 2){
			   		List<String> playerWarps = warpService.getPlayerWarps(player).keySet().stream().collect(Collectors.toList());
			        return messageService.autoComplete(playerWarps, args[1]);
			    }
			   	break;
	
			case "rename":
			   	if(args.length == 2){
			   		List<String> playerWarps = warpService.getPlayerWarps(player).keySet().stream().collect(Collectors.toList());
			        return messageService.autoComplete(playerWarps, args[1]);
			    }
			   	break;
				
			case "setowner":
			   	if(args.length == 2){
			   		List<String> playerWarps = warpService.getPlayerWarps(player).keySet().stream().collect(Collectors.toList());
			        return messageService.autoComplete(playerWarps, args[1]);
			    }

			   	if(args.length == 3){
			   		List<String> players = new ArrayList<String>();
			   		for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			   			players.add(onlinePlayer.getName());
			   		}
			        return messageService.autoComplete(players, args[2]);
			    }
			   	break;
			}
		}
		return new ArrayList<String>();
	}
}
