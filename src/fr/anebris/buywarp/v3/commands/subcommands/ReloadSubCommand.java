package fr.anebris.buywarp.v3.commands.subcommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.interfaces.IConfigRepository;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.ISubCommands;
import fr.anebris.buywarp.v3.interfaces.IWarpService;

public class ReloadSubCommand implements ISubCommands {
	
	private final IWarpService warpService;
	private final IMessageService messageService;
	private final IConfigRepository configRepository;
	
	public ReloadSubCommand(IWarpService warpService, IMessageService messageService, IConfigRepository configRepository) {
		this.warpService = warpService;
		this.messageService = messageService;
		this.configRepository = configRepository;
	}
	
	@Override
	public boolean executeCommand(CommandSender sender, Command command, String label, String[] args) throws Exception {
		Player player = (Player) sender;
		
		if (args.length != 1) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.CMD_NOT_FOUND, true), false);
			return true;
		}
		
		if (!player.hasPermission("bw.admin.reload")) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NO_PERMISSION, true), false);		
			return true;
		}
		
        configRepository.reloadConfig();
        warpService.initializeWarps();
		messageService.sendMessage(player, messageService.getMessage(ConfigMessage.CMD_RELOAD, true), false);		
		return true;
	}

}
