package fr.anebris.buywarp.v3.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.ISubCommands;
import fr.anebris.buywarp.v3.interfaces.IWarpService;

public class SetOwnerSubCommand implements ISubCommands {

		private final IWarpService warpService;
		private final IMessageService messageService;

		public SetOwnerSubCommand(IWarpService warpService, IMessageService messageService) {
			this.warpService = warpService;
			this.messageService = messageService;
		}

		@Override
		public boolean executeCommand(CommandSender sender, Command command, String label, String[] args) throws Exception {
			Player player = (Player) sender;
			
			if (args.length != 3) {
				messageService.sendMessage(player, messageService.getMessage(ConfigMessage.CMD_NOT_FOUND, true), false);
				return true;
			}
			
			if (!player.hasPermission("bw.setowner")) {
				messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NO_PERMISSION, true), false);		
				return true;
			}
			
			String warp_name = args[1];
			
			if (!warpService.isExistingWarp(warp_name)) {
				messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_DOESNT_EXIST, true), false);
				return true;
			}
			
			if (warpService.getWarpOwner(warp_name).compareTo(player.getUniqueId()) != 0 && !player.hasPermission("bw.admin.setowner")) {
				messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NOT_THE_OWNER, true), false);
				return true;
			}
			
			Player new_owner = Bukkit.getPlayerExact(args[2]);

			if (player == new_owner) {
				messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_ALREADY_OWNED, true), false);
				return true;
			}
			
			if (!Bukkit.getOnlinePlayers().contains(new_owner)) {
				messageService.sendMessage(player, messageService.getMessage(ConfigMessage.PLAYER_IS_OFFLINE, true).replace("{player}", args[2]), false);
				return true;
			}
			
			int warp_limit = warpService.getWarpLimit(new_owner);
			if (warpService.getPlayerWarps(new_owner).size() + 1 > warp_limit && warp_limit != -1) {
				messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_LIMITED, true).replace("{warp_limit}", String.valueOf(warp_limit)), false);
				return true;
			}

			warpService.setWarpOwner(warp_name, new_owner.getUniqueId());
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_SET_OWNER, true).replace("{warp_name}", warp_name).replace("{new_owner}", new_owner.getDisplayName()), false);
			messageService.sendMessage(new_owner, messageService.getMessage(ConfigMessage.WARP_NEW_OWNER, true).replace("{warp_name}", warp_name), false);
			return true;
		
		}
	}

