package fr.anebris.buywarp.v3.commands.subcommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.ISubCommands;
import fr.anebris.buywarp.v3.interfaces.IWarpService;

public class TestSubCommand implements ISubCommands {

	private final IWarpService warpService;
	private final IMessageService messageService;

	public TestSubCommand(IWarpService warpService, IMessageService messageService) {
		this.warpService = warpService;
		this.messageService = messageService;
	}
	
	@Override
	public boolean executeCommand(CommandSender sender, Command command, String label, String[] args) throws Exception {
		
		Player player = (Player) sender;
		
		if (!player.hasPermission("bw.testcmd")) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NO_PERMISSION, true), false);
			return true;
		}
		
		warpService.getWarps();
		return false;
	}

}
