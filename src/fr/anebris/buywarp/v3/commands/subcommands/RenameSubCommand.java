package fr.anebris.buywarp.v3.commands.subcommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.ISubCommands;
import fr.anebris.buywarp.v3.interfaces.IWarpService;

public class RenameSubCommand implements ISubCommands {

	private final IWarpService warpService;
	private final IMessageService messageService;

	public RenameSubCommand(IWarpService warpService, IMessageService messageService) {
		this.warpService = warpService;
		this.messageService = messageService;
	}
	
	@Override
	public boolean executeCommand(CommandSender sender, Command command, String label, String[] args) throws Exception {
		Player player = (Player) sender;
		
		if (args.length != 3) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.CMD_NOT_FOUND, true), false);
			return true;
		}
		
		if (!player.hasPermission("bw.rename")  ) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NO_PERMISSION, true), false);
			return true;
		}
		
		String old_name = args[1];
		String new_name = args[2];
		
		if (!warpService.isExistingWarp(old_name)) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_DOESNT_EXIST, true), false);
			return true;
		}
		
		if (warpService.getWarpOwner(old_name).compareTo(player.getUniqueId() ) != 0 && !player.hasPermission("bw.admin.rename")) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NOT_THE_OWNER, true), false);
			return true;
		}

		if (!(warpService.isValidName(new_name))) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_NAME_IS_INVALID, true), false);
			return true;
		}

		warpService.renameWarp(old_name, new_name);
		messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_RENAMED, true), false);
		return true;
	}

}
