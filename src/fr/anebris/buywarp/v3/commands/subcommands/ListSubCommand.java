package fr.anebris.buywarp.v3.commands.subcommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.interfaces.IInventoryService;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.ISubCommands;

public class ListSubCommand implements ISubCommands {
	
	private final IMessageService messageService;
	private final IInventoryService inventoryService;
	
	public ListSubCommand(IMessageService messageService, IInventoryService inventoryService) {
		this.inventoryService = inventoryService;
		this.messageService = messageService;
		
	}
	
	@Override
	public boolean executeCommand(CommandSender sender, Command command, String label, String[] args) throws Exception {
		Player player = (Player) sender;

		if (args.length != 1) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.CMD_NOT_FOUND, true), false);
			return true;
		}
		
		if (!player.hasPermission("bw.list")) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NO_PERMISSION, true), false);		
			return true;
		}
		
		player.openInventory(inventoryService.getWarpListInventory());
		return true;
	}

}
