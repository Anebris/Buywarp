package fr.anebris.buywarp.v3.commands.subcommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Consumer;

import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.eventlisteners.ConfirmPurchaseEvent;
import fr.anebris.buywarp.v3.interfaces.IConfigRepository;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.ISubCommands;
import fr.anebris.buywarp.v3.interfaces.IWarpService;

public class RenewSubCommand implements ISubCommands {
	
	private final IWarpService warpService;
	private final IMessageService messageService;
	private final IConfigRepository configRepository;
	
	public RenewSubCommand(IWarpService warpService, IMessageService messageService, IConfigRepository configRepository) {
		this.warpService = warpService;
		this.messageService = messageService;
		this.configRepository = configRepository;
	}
	
	@Override
	public boolean executeCommand(CommandSender sender, Command command, String label, String[] args) throws Exception {
		Player player = (Player) sender;
		
		if (args.length != 2) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.CMD_NOT_FOUND, true), false);
			return true;
		}
		
		if (!player.hasPermission("bw.renew")) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NO_PERMISSION, true), false);		
			return true;
		}
		
		String warp_name = args[1];
		
		if (!warpService.isExistingWarp(warp_name)) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_DOESNT_EXIST, true), false);
			return true;
		}
		
		if (warpService.getWarpOwner(warp_name).compareTo(player.getUniqueId()) != 0) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NOT_THE_OWNER, true), false);
			return true;
		}
		
		double warp_cost = configRepository.getWarpCost(configRepository.getPlayerType(player));
		
		new ConfirmPurchaseEvent(player, messageService.getMessage(ConfigMessage.INV_CONFIRM_RENEW, false).replace("{warp_name}", warp_name), warp_cost, new Consumer<Boolean>() {

            @Override
            public void accept(Boolean bool) {

                if (bool) {
               
            		if (!(warpService.playerHasEnoughMoney(player, warp_cost))) {
            			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NOT_ENOUGH_MONEY, true), false);
            			player.closeInventory();
            			return;
            		}
            		
            		warpService.renewWarp(warp_name, player);
            		warpService.takeMoney(player, warp_cost);
            		messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_RENEWED, true).replace("{warp_name}", warp_name).replace("{warp_cost}", String.valueOf((int) warp_cost)), false);
                }
                player.closeInventory();
            }
        });
		return true;
	
	}
}
