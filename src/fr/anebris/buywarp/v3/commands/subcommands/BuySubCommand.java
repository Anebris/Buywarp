package fr.anebris.buywarp.v3.commands.subcommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Consumer;

import fr.anebris.buywarp.v3.enums.ConfigMessage;
import fr.anebris.buywarp.v3.eventlisteners.ConfirmPurchaseEvent;
import fr.anebris.buywarp.v3.interfaces.IConfigRepository;
import fr.anebris.buywarp.v3.interfaces.IMessageService;
import fr.anebris.buywarp.v3.interfaces.ISubCommands;
import fr.anebris.buywarp.v3.interfaces.IWarpService;

public class BuySubCommand implements ISubCommands {
	
	private final IWarpService warpService;
	private final IMessageService messageService;
	private final IConfigRepository configRepository;

	public BuySubCommand(IWarpService warpService, IMessageService messageService, IConfigRepository configRepository) {
		this.warpService = warpService;
		this.messageService = messageService;
		this.configRepository = configRepository;
	}
	
	@Override
	public boolean executeCommand(CommandSender sender, Command command, String label, String[] args) throws Exception {
		Player player = (Player) sender;
		
		if (args.length != 2) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.CMD_NOT_FOUND, true), false);
			return true;
		}
		
		String warp_name = args[1];
		
		if (warpService.isExistingWarp(warp_name)) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_ALREADY_IN_USE, true), false);
			return true;
		}
		
		if (!(warpService.isValidName(warp_name))) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_NAME_IS_INVALID, true), false);
			return true;
		}

		String playerType = configRepository.getPlayerType(player);
		int warp_limit = warpService.getWarpLimit(player);
		
		if (warpService.getPlayerWarps(player).size() + 1 > warp_limit && warp_limit != -1) {
			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_LIMITED, true).replace("{warp_limit}", String.valueOf(warp_limit)), false);
			return true;
		}
		
		double warp_cost = configRepository.getWarpCost(playerType);
		
		new ConfirmPurchaseEvent(player, messageService.getMessage(ConfigMessage.INV_CONFIRM_BUY, false).replace("{warp_name}", warp_name), warp_cost, new Consumer<Boolean>() {

            @Override
            public void accept(Boolean bool) {

            	if (bool) {
               
            		if (!(warpService.playerHasEnoughMoney(player, warp_cost))) {
            			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.NOT_ENOUGH_MONEY, true), false);
            		} else {
            			try { warpService.setWarp(warp_name, player.getUniqueId(), player.getLocation()); } catch (Exception e) { e.printStackTrace();}
            			warpService.takeMoney(player, warp_cost);
            			messageService.sendMessage(player, messageService.getMessage(ConfigMessage.WARP_CREATED, true).replace("{warp_name}", warp_name).replace("{warp_cost}", String.valueOf((int) warp_cost)), false);
            		}           		
            	}
                player.closeInventory();
            }
        });
		return true;
	}

}
